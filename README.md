# osb_switch_proxy_services using OSB Configuration feature and WLST

The simplest way to do it is using the OSB Configuration deploing the FIX via WLST


The OSB 12.x allow you to create the OSB Configuration File directly from "sbconsole". The Configuration File is an XML that contains the customization of the services. You can export this Configuration and then Edit the file and apply it again in OSB.

In this case we used this feature to switch to enable/disable and switch ENDPoint URI of two services.

1. The first step is to have both services in the OSB everyone with its "ENDPoint" URI.
2. Then we have to generate the "OSB Configuration File" from "sbconsole", "ADMIN"."Configuration"."Create Configuration File", then choose the two Proxy Services you want export. An XML file will be exported locally on your PC.
3. Rename this file in OSBCustomizationFile_rollback.xml and maintain the  "Service URI" and "Service Operational Settings" (remove all the entry in the XML you are not interested to change). This is an example in "OSBCustomizationFile_rollback.xml"
4. Copy the OSBCustomizationFile_rollback.xml in OSBCustomizationFile_apply.xml
5. Now Edit the OSBCustomizationFile_apply.xml and change the values "Service URI" and "Service Operational Settings". This is an example in "OSBCustomizationFile_apply.xml"
Then you can apply the "OSB Configuration" file using WLST


```
[oracle@soa-training osb_switch_proxy_services]$ /oracle/fmw/soa12214/soa/common/bin/wlst.sh switchProxyServiceViaConfigurationFile.py  OSBCustomizationFile_apply.xml
Initializing WebLogic Scripting Tool (WLST) ...
Welcome to WebLogic Server Administration Scripting Shell
Type help() for help on available commands
This is the used OSB Configuration file used: OSBCustomizationFile_apply.xml
Connecting to t3://localhost:7101 with userid weblogic ...
Successfully connected to Admin Server "DefaultServer" that belongs to domain "DefaultDomain".
Warning: An insecure protocol was used to connect to the server.
To ensure on-the-wire security, the SSL port or Admin port should be used instead.
Location changed to domainRuntime tree. This is a read-only tree
with DomainMBean as the root MBean.
For more help, use help('domainRuntime')
Loading customization File OSBCustomizationFile_apply.xml
Disconnected from weblogic server: DefaultServer
Successfully Completed customization
```



To rollback to the previus configuration you can execute the same script using the rollback Configuration file:


`[oracle@soa-training osb_switch_proxy_services]$ /oracle/fmw/soa12214/soa/common/bin/wlst.sh switchProxyServiceViaConfigurationFile.py  OSBCustomizationFile_rollback.xml`

# There is another script version that get two OSB customization files as input, it was to resolve the problem we had in cedacri with rollback, we added another session setting the URI to a temporary value.

```
switchProxyServiceViaConfigurationFileMulti.py
CCMO_abilita_microservice.sh
CCMO_disabilita_microservice.sh
```

We opened an SR to address the problem and resolve it definitively
```
SR 3-26780197541 : Problem changing OSB Proxy Service "Service URI" via customization script
```
