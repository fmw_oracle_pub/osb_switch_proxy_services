import sys
from java.io import FileInputStream
from com.bea.wli.config.customization import Customization
from com.bea.wli.sb.management.importexport import ALSBImportOperation
from com.bea.wli.sb.management.configuration import ALSBConfigurationMBean
from com.bea.wli.sb.management.configuration import SessionManagementMBean

def customizeOSBService(customizationFile):
    SessionMBean = None
    try:
        connect('weblogic', 'welcome1', 't3://localhost:7101')
        #userConfigFile=userFilePath+'/.weblogicCredential_'+domain+'_'+env+'.properties'
        #userKeyFile=userFilePath+'/.weblogicKey_'+domain+'_'+env+'.properties'
        #connect(userConfigFile=userConfigFile,userKeyFile=userKeyFile,url='t3://' + admin_ip + ':' + admin_port,timeout='60000')

        domainRuntime()
        sessionName = String("Customization"+Long(System.currentTimeMillis()).toString())
        SessionMBean = findService("SessionManagement", "com.bea.wli.sb.management.configuration.SessionManagementMBean")
        SessionMBean.createSession(sessionName)
        OSBConfigurationMBean = findService(String("ALSBConfiguration.").concat(sessionName), "com.bea.wli.sb.management.configuration.ALSBConfigurationMBean")
        print 'Loading customization File', customizationFile
        iStream = FileInputStream(customizationFile)
        customizationList = Customization.fromXML(iStream)       
        OSBConfigurationMBean.customize(customizationList)
        SessionMBean.activateSession(sessionName, "Complete customization using wlst")
        disconnect()

    except:
        print "Unexpected error:", sys.exc_info()[0]

        if SessionMBean != None:
            SessionMBean.discardSession(sessionName)
        raise

if  len(sys.argv) != 2:
  print "Wrong Input Parametes, please specify the OSB Configuration file to apply"
  print "wlst.sh switchProxyServiceViaConfigurationFile.py OSBCustomizationFile_apply.xml"
  exit()

osb_configuration_file=sys.argv[1]
print 'This is the used OSB Configuration file used: '+osb_configuration_file
       
customizeOSBService(osb_configuration_file)
       
print 'Successfully Completed customization'   
