import sys , traceback
from time import sleep
from com.bea.wli.sb.management.configuration import SessionManagementMBean
from com.bea.wli.sb.management.configuration import ALSBConfigurationMBean
from com.bea.wli.config import Ref
#from com.bea.wli.monitoring import StatisticType
from com.bea.wli.sb.util import Refs
from com.bea.wli.sb.management.configuration import CommonServiceConfigurationMBean

def setProxyStatus(proxyNames,status):
 try:
  sessionName  = "SetProxyStateSession_" + str(System.currentTimeMillis())
  sessionMBean = findService(SessionManagementMBean.NAME, SessionManagementMBean.TYPE)
  sessionMBean.createSession(sessionName)
  pxyConf='ProxyServiceConfiguration.' + sessionName
  mbean = findService(pxyConf, 'com.bea.wli.sb.management.configuration.ProxyServiceConfigurationMBean')
  prxNames = proxyNames.split(',')
  for aName in prxNames:
   prxName = aName.strip()
   proxyPath=prxName.split('/')
   serviceRef = Ref.makeRef(Refs.PROXY_SERVICE_TYPE, proxyPath)
   if status=='disable':
    mbean.disableService(serviceRef)
    print 'Disabled proxy '+prxName
   else:
    mbean.enableService(serviceRef)
    print 'Enabled proxy '+prxName
  sessionMBean.activateSession(sessionName,status+'d '+proxyNames)
 except:
  print 'Got Error while disabling proxy'
  apply(traceback.print_exception, sys.exc_info())
  dumpStack()    
  

print ''
if len(sys.argv) != 2:
 print "Invalid number of arguments."
 print 'Usage: switchProxyServices [apply|rollback]'
 exit()

operation=sys.argv[1]
loadProperties('./config.properties')
connect(userName, password,'t3://'+adminHost+':'+adminPort)
domainRuntime()
if operation=='apply':
  print ' Disabling proxies:' + DisableOsbProxyNames
  setProxyStatus(DisableOsbProxyNames,'disable')
  print ''
  print ' Enabling proxies:' + EnableOsbProxyNames
  setProxyStatus(EnableOsbProxyNames,'enable')
if operation=='rollback':
  print ' Rollback Disabling proxies:' + DisableOsbProxyNames
  setProxyStatus(DisableOsbProxyNames,'enable')
  print ''
  print ' Rollback Enabling proxies:' + EnableOsbProxyNames
  setProxyStatus(EnableOsbProxyNames,'disable')


print 'Finished.'
